FROM alpine:3.4

RUN apk add --update ca-certificates openssl nginx s6 && \
    rm -rf /var/cache/apk/*

COPY s6 /s6
COPY nginx /etc/nginx/

EXPOSE 80 443

CMD ["s6-svscan", "/s6"]
