 ![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)

This is a very tiny [Alpine Linux](https://www.alpinelinux.org/)-based Docker container for running
nginx. It uses S6 for basic, correct process management.

Inspired by:
- [nginxinc/docker-nginx](https://github.com/nginxinc/docker-nginx)
- [dnephin/alpine-s6](https://github.com/dnephin/alpine-s6)

Pre-built containers are available in the
[registry](https://gitlab.com/dcreemer/nginx-alpine/container_registry) on Gitlab.
