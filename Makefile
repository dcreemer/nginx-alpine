REGISTRY=registry.gitlab.com/dcreemer
NAME=nginx-alpine

build:
	docker build -t $(REGISTRY)/$(NAME) .

push:
	docker push $(REGISTRY)/$(NAME)

test:
	docker run --name=$(NAME) -d -p 8080:80 $(REGISTRY)/$(NAME)
